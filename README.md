# XFWM-Small-Gaps

[![Casual Maintenance Intended](https://casuallymaintained.tech/badge.svg)](https://casuallymaintained.tech/)

quick modification of [XFWM-Gaps-No-Titlebar](https://github.com/Heclalava/XFWM4-Gaps-No-Titlebar) with slightly smaller gaps

copy folder into `~/.themes/`
